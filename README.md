# Reminder

Une API REST pour créer des évènements à se faire rappeler par mail. Les rappels
ponctuels et à répétition (en nombre de jours) sont supportés.

### Prerequisites

Django REST framework
```
pip install djangorestframework
```

django-cors-headers
```
pip install django-cors-headers
```

python-dotenv
```
pip install python-dotenv
```

### Installing

Via SSH :
```
git@gitlab.com:olhybrius/reminder.git
```

ou HTTPS :
```
https://gitlab.com/olhybrius/reminder.git
```

## Deployment

TODO

## Built With

* [Django REST framework](https://www.django-rest-framework.org/) - The web framework used
* [pip](https://pypi.org/project/pip/) - Dependency Management

## Authors

* **Olivier THIEL** 
* **Maxime ALFOSEA**

## License
TODO