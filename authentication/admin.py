from django.contrib import admin

from authentication.models.profile import Profile

admin.site.register(Profile)