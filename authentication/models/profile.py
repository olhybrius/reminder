from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    """ Utilisateur de l'application """
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                related_name='user')

    def __str__(self):
        return f'{self.id} - {self.user.username} ({self.user.email})'
