from rest_framework import serializers

from django_reminder.decorators import key_error


class PasswordUpdateSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()
    new_password2 = serializers.CharField()

    @key_error
    def validate(self, data):
        if not self.context['request'].user.check_password(
                data['old_password']):
            raise serializers.ValidationError(
                {'old_password': "Mot de passe incorrect !"})
        if not data['new_password'] == data['new_password2']:
            raise serializers.ValidationError(
                {'new_password':
                     'les deux mots de passe ne correspondent pas.'})

        return data
