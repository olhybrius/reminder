from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from authentication.models.profile import Profile


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username',
                                     validators=[UniqueValidator(
                                         queryset=User.objects.all())])
    email = serializers.EmailField(source='user.email',
                                   validators=[UniqueValidator(
                                       queryset=User.objects.all())])

    class Meta:
        model = Profile
        fields = ('id', 'username', 'email')
