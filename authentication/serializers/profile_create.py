from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from rest_framework import serializers

from authentication.models.profile import Profile
from authentication.serializers.profile import ProfileSerializer
from django_reminder.decorators import key_error


class ProfileCreateSerializer(ProfileSerializer):
    password = serializers.CharField(source='user.password', write_only=True)

    class Meta(ProfileSerializer.Meta):
        fields = ProfileSerializer.Meta.fields + ('password',)

    @key_error
    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['user']['username'],
            email=validated_data['user']['email'])
        user.set_password(validated_data['user']['password'])
        user.save()
        profile = Profile.objects.create(user=user)

        return profile
