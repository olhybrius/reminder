from authentication.serializers.profile import ProfileSerializer
from django_reminder.decorators import key_error


class ProfileUpdateSerializer(ProfileSerializer):

    @key_error
    def update(self, instance, validated_data):
        user = instance.user
        user.username = validated_data['user']['username']
        user.email = validated_data['user']['email']
        user.save()
        return instance
