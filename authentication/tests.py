from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase


class ProfileTests(APITestCase):

    def setUp(self):
        self.url = reverse('profile-list')
        self.correct_username = 'john_doe'
        self.correct_password = 'foobar'
        self.email = 'toto@toto.fr'
        self.user = User.objects.create_user(username=self.correct_username,
                                             password=self.correct_password,
                                             email=self.email)
        self.wrong_username = 'wrong'
        self.wrong_password = 'wrong'
        self.faker = Faker()

    def test_get_profiles_without_credentials(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_profiles_with_wrong_username_and_password(self):
        self.client.login(username=self.wrong_username,
                          password=self.wrong_password)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_profiles_with_correct_username_and_wrong_password(self):
        self.client.login(username=self.correct_username,
                          password=self.wrong_password)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_profiles_with_correct_credentials(self):
        self.client.login(username=self.correct_username,
                          password=self.correct_password)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_create_profile_with_valid_data(self):
        data = {'username': self.faker.first_name(),
                'email': self.faker.email(),
                'password': self.correct_password}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_profile_with_existing_username(self):
        data = {'username': self.correct_username, 'email': self.faker.email(),
                'password': self.correct_password}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_profile_with_existing_email(self):
        self.test_create_profile_with_valid_data()

        data = {'username': self.faker.first_name(), 'email': self.email,
                'password': self.correct_password}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_profiles_return_only_auth_profile(self):
        username = self.faker.first_name()
        email = self.faker.email()

        username2 = self.faker.first_name()
        email2 = self.faker.email()

        data = {'username': username, 'email': email,
                'password': self.correct_password}

        data2 = {'username': username2, 'email': email2,
                 'password': self.correct_password}

        first_post_response = self.client.post(self.url, data)
        self.client.post(self.url, data2)
        self.client.login(username=username,
                          password=self.correct_password)
        get_response = self.client.get(self.url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(get_response.data), 1)
        self.assertEqual(get_response.json()[0],
                         {'id': first_post_response.data['id'],
                          'username': username,
                          'email': email})

    def test_update_profile_after_create(self):
        data = {'username': self.faker.first_name(),
                'email': self.faker.email(),
                'password': self.correct_password}

        post_response = self.client.post(self.url, data)
        created_id = post_response.data['id']
        username = post_response.data['username']

        updated_data = {'id': created_id, 'username': self.faker.first_name(),
                        'email': self.faker.email()}

        self.client.login(username=username, password=self.correct_password)
        response = self.client.put(
            reverse('profile-detail', args=[created_id]),
            updated_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, updated_data)

    def test_update_password_without_credentials(self):
        data = {'username': self.faker.first_name(),
                'email': self.faker.email(),
                'password': self.correct_password}

        post_response = self.client.post(self.url, data)
        created_id = post_response.data['id']
        new_password = self.faker.password()

        new_data = {'old_password': self.correct_password,
                    'new_password': new_password,
                    'new_password2': new_password}

        response = self.client.put(
            reverse('profile-update-password', args=[created_id]),
            new_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_password_with_correct_credentials(self):
        username = self.faker.first_name()
        email = self.faker.email()
        password = self.faker.password()

        data = {'username': username,
                'email': email,
                'password': password}

        post_response = self.client.post(self.url, data)
        created_id = post_response.data['id']
        new_password = self.faker.password()

        new_data = {'old_password': password,
                    'new_password': new_password,
                    'new_password2': new_password}

        self.client.login(username=username, password=password)
        response = self.client.put(
            reverse('profile-update-password', args=[created_id]),
            new_data)
        print()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            self.client.login(username=username, password=password))
        self.assertTrue(
            self.client.login(username=username, password=new_password))
