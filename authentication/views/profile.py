from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from authentication.models.profile import Profile
from reminder.permissions import IsPostOrIsAuthenticated
from authentication.serializers.password_update import PasswordUpdateSerializer
from authentication.serializers.profile_create import ProfileCreateSerializer
from authentication.serializers.profile_update import ProfileUpdateSerializer


class ProfileViewSet(ModelViewSet):
    permission_classes = [IsPostOrIsAuthenticated, ]

    def get_serializer_class(self):
        if self.action == 'update':
            return ProfileUpdateSerializer
        else:
            return ProfileCreateSerializer

    def get_queryset(self):
        return Profile.objects.filter(user=self.request.user)

    @action(detail=True, methods=['put'])
    def update_password(self, request, pk=None):
        user = self.get_object().user
        serializer = PasswordUpdateSerializer(data=request.data,
                                              context={'request': request})
        if serializer.is_valid():
            user.set_password(serializer.data['new_password'])
            user.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)