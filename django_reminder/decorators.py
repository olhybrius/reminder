from rest_framework import serializers


def key_error(fn):
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except KeyError as e:
            raise serializers.ValidationError(f'Le champ {e} est manquant ou '
                                              f'invalide')

    return wrapper
