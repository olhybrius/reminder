"""django_reminder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework.routers import DefaultRouter

from reminder.views.field_type import FieldTypeViewSet, KermitView
from reminder.views.item_model import ItemModelViewSet
from authentication.views.profile import ProfileViewSet
from reminder.views.reminder_item import ReminderItemViewSet

router = DefaultRouter()
router.register(r'profiles', ProfileViewSet, 'profile')
router.register(r'item-models', ItemModelViewSet, 'item-model')
router.register(r'reminder-items', ReminderItemViewSet, 'reminder-item')
router.register(r'field-types', FieldTypeViewSet, 'field-type')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('kermit/', KermitView.as_view()),
    re_path(r'^api-auth/', include('rest_framework.urls'))
]

urlpatterns = [re_path(r'^api/v1/', include(urlpatterns))]
