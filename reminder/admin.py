from django.contrib import admin
from reminder.models.field import Field
from reminder.models.field_type import FieldType
from reminder.models.item_fields import ItemFields
from reminder.models.item_model import ItemModel
from reminder.models.reminder_item import ReminderItem

admin.site.register(ItemModel)
admin.site.register(ReminderItem)
admin.site.register(Field)
admin.site.register(ItemFields)
admin.site.register(FieldType)
