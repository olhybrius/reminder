from authentication.models.profile import Profile
from reminder.models.reminder_item import ReminderItem
from reminder.utils import get_today_date
from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    help = 'Command to send email to users'
    today = get_today_date()

    def handle(self, *args, **options):
        print(Command.today)
        profiles = [profile for profile in Profile.objects.all()]

        [[self.send_mails(profile, event) for event in
          self.get_profile_events(profile)
          ] for
         profile in profiles]

    def send_mails(self, profile, event):
        if event.daysInterval != 0 and event.reminderStartDate <= Command.today:
            self.handle_repetitive_event(profile, event)
        elif event.reminderStartDate == Command.today:
            self.handle_one_time_event(profile, event)

    def handle_repetitive_event(self, profile, event):
        difference_in_days = (Command.today - event.reminderStartDate).days

        if difference_in_days % event.daysInterval == 0:
            self.send_reminder_mail(profile, event)

    def handle_one_time_event(self, profile, event):
        self.send_reminder_mail(profile, event)

    def get_profile_events(self, profile):
        return list(ReminderItem.objects.filter(model__profile=profile))

    def make_mail_message(self, profile, event):
        message = f'Bonjour {profile.user.username} ! N\'oubliez pas ' \
                  f'votre évènement {event.model.label} aujourd\'hui' \
                  f' ! '

        return message

    def send_reminder_mail(self, profile, event):
        message = self.make_mail_message(profile, event)
        send_mail('Rappel !', message, settings.EMAIL_HOST_USER,
                  [profile.user.email])
