from django.db import models
from reminder.models.item_model import ItemModel


class Field(models.Model):
    """ Champ relatif à un modèle d'évènement à rappeler """

    model = models.ForeignKey(ItemModel, on_delete=models.CASCADE,
                              related_name='fields')
    type = models.ForeignKey('FieldType', on_delete=models.CASCADE,
                             related_name='type')
    label = models.CharField(default='', max_length=50,
                             blank=False, null=False)

    def __str__(self):
        return f'{self.id} - {self.label} (type : {self.type})'
