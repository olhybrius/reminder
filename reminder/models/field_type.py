from django.db import models


class FieldType(models.Model):
    """ Types de champs possibles """
    label = models.CharField(default='', max_length=50, unique=True,
                             blank=False, null=False)

    def __str__(self):
        return f'{self.id} - {self.label}'