from django.db import models
from reminder.models.field import Field
from reminder.models.reminder_item import ReminderItem


class ItemFields(models.Model):
    """ Objet précisant la valeur d'un champ d'un événement à rappeler """

    item = models.ForeignKey(ReminderItem, on_delete=models.CASCADE,
                             related_name='fields')
    field = models.ForeignKey(Field, on_delete=models.CASCADE,
                              related_name='items')
    value = models.CharField(default='', max_length=500,
                             blank=True, null=False)

    class Meta:
        unique_together = ['item', 'field']

    def __str__(self):
        return f'{self.item} - {self.field} {self.value}'