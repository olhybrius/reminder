from django.db import models


class ItemModel(models.Model):
    """ Modèle d'évènement à rappeler """
    profile = models.ForeignKey('authentication.Profile',
                                on_delete=models.CASCADE,
                                related_name='models')
    label = models.CharField(default='', max_length=50,
                             blank=False, null=False)

    def __str__(self):
        return f'{self.id} - {self.label} => champs : ' \
               f'{[str(f) for f in self.fields.all()]}; ' \
               f'profile : {self.profile} '
