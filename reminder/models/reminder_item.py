from django.db import models
from datetime import date
from reminder.models.item_model import ItemModel


class ReminderItem(models.Model):
    """ Evènement à rappeler """
    reminderStartDate = models.DateField(default=date.today)
    daysInterval = models.IntegerField(default=0)
    model = models.ForeignKey(ItemModel, on_delete=models.CASCADE,
                              related_name='model')

    def __str__(self):
        return f'{self.id} - modèle : {self.model} ; Date de début : ' \
               f'{self.reminderStartDate} ; Répéter tous les ' \
               f'{self.daysInterval} jours'
