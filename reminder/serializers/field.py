from rest_framework import serializers
from reminder.models.field import Field


class FieldSerializer(serializers.ModelSerializer):
    # required=False permet de gérer la création (absence d'id) et la mise à
    # jour depuis le serializer parent (ItemModelSerializer)
    id = serializers.IntegerField(required=False)
    type = serializers.CharField(source='type.label')

    class Meta:
        model = Field
        fields = ('id', 'label', 'type')