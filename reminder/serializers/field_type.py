from rest_framework import serializers

from reminder.models.field_type import FieldType


class FieldTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldType
        fields = ('id', 'label')