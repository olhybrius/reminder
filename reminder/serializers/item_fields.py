from rest_framework import serializers

from reminder.models.item_fields import ItemFields


class ItemFieldsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='field.id')
    label = serializers.CharField(source='field.label')
    type = serializers.CharField(source='field.type.label')

    class Meta:
        model = ItemFields
        fields = ('id', 'label', 'type', 'value')