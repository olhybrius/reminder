from rest_framework import serializers

from django_reminder.decorators import key_error
from reminder.models.field import Field
from reminder.models.field_type import FieldType
from reminder.models.item_fields import ItemFields
from reminder.models.item_model import ItemModel
from authentication.models.profile import Profile
from reminder.serializers.field import FieldSerializer


class ItemModelSerializer(serializers.ModelSerializer):
    fields = FieldSerializer(many=True)

    class Meta:
        model = ItemModel
        fields = ('id', 'label', 'fields')

    @key_error
    def create(self, validated_data):
        fields_data = validated_data.pop('fields')
        profile = Profile.objects.get(user=self.context['request'].user.id)
        item_model = ItemModel.objects.create(profile=profile,
                                              **validated_data)

        for field_data in fields_data:
            Field.objects.create(model=item_model,
                                 type=FieldType.objects.get(label=
                                                            field_data['type']
                                                            ['label']),
                                 label=field_data['label'])

        return item_model

    @key_error
    def update(self, instance, validated_data):
        fields_id = list(field.id for field in instance.fields.all())
        fields_data = validated_data['fields']
        fields_data_ids = list(field_data.get('id')
                               for field_data in fields_data
                               if field_data.get('id') is not None)

        deleted_fields_ids = list(
            set(fields_id) - set(fields_data_ids))
        Field.objects.filter(id__in=deleted_fields_ids).delete()
        ItemFields.objects.filter(field_id__in=deleted_fields_ids).delete()

        # Gestion des modèles implémentés
        if ItemFields.objects.filter(field_id__in=fields_id).exists():

            for field_data in fields_data:
                try:
                    # utilisation de get plutôt que [] car l'id peut ne pas
                    # être renseigné (création de champ suppémentaire)
                    field = Field.objects.get(id=field_data.get('id'))
                    if field.type.label != field_data['type']['label']:
                        raise serializers.ValidationError(
                            "Impossible de modifier le type d'un champ"
                            " d'un modèle implémenté !")
                except Field.DoesNotExist:
                    try:
                        field = Field.objects.create(model=instance,
                                                     type=FieldType.objects
                                                     .get(label=field_data
                                                     ['type']['label']))
                    except FieldType.DoesNotExist:
                        raise serializers.ValidationError(
                            "Type de champ invalide.")

                    # TODO rename model related to item
                    for item in instance.model.all():
                        ItemFields.objects.create(item=item, field=field)

                field.label = field_data['label']
                field.save()
        else:
            for field_data in fields_data:
                field_label = field_data['label']

                try:
                    field_type = FieldType.objects.get(
                        label=field_data['type']['label'])

                    if field_data.get('id') is None \
                            or field_data['id'] not in fields_id:
                        Field.objects.create(type=field_type,
                                             label=field_label,
                                             model=instance)

                    else:
                        field = Field.objects.get(id=field_data['id'])
                        field.label = field_label
                        field.type = field_type
                        field.save()
                except FieldType.DoesNotExist:
                    raise serializers.ValidationError(
                        "Type de champ invalide.")
        return instance
