from django.db.models import Q
from rest_framework import serializers

from django_reminder.decorators import key_error
from reminder.models.field import Field
from reminder.models.item_fields import ItemFields
from reminder.models.item_model import ItemModel
from reminder.models.reminder_item import ReminderItem
from reminder.serializers.item_fields import ItemFieldsSerializer


class ReminderItemSerializer(serializers.ModelSerializer):
    fields = ItemFieldsSerializer(many=True)
    model_id = serializers.CharField(source='model.id')

    class Meta:
        model = ReminderItem
        fields = (
            'id', 'model_id', 'fields', 'reminderStartDate', 'daysInterval')

    @key_error
    def create(self, validated_data):
        fields_data = validated_data.pop('fields')
        model = ItemModel.objects.get(id=validated_data.pop('model')['id'])

        fields_id = list(field.id for field in model.fields.all())
        fields_data_ids = list(field_data['field']['id']
                               for field_data in fields_data)

        if len(fields_id) != len(fields_data_ids) or set(fields_id) != set(
                fields_data_ids):
            raise serializers.ValidationError(
                'L\'objet ne correspond pas au modèle !')

        item = ReminderItem.objects.create(model=model, **validated_data)

        for field_data in fields_data:
            ItemFields.objects.create(
                item=item,
                field=Field.objects.get(
                    id=field_data['field']['id']),
                value=field_data['value'])
        return item

    @key_error
    def update(self, instance, validated_data):
        fields_data = validated_data.pop('fields')

        instance.reminderStartDate = validated_data['reminderStartDate']
        instance.daysInterval = validated_data['daysInterval']
        instance.save()

        fields_id = list(field.id for field in instance.model.fields.all())
        fields_data_ids = list(field_data['field']['id']
                               for field_data in fields_data)

        if len(fields_id) != len(fields_data_ids) \
                or set(fields_id) != set(fields_data_ids):
            raise serializers.ValidationError(
                'L\'objet ne correspond pas au modèle !')

        for field_data in fields_data:
            criterion1 = Q(field_id=field_data['field']['id'])
            criterion2 = Q(item_id=instance.id)
            item_field = ItemFields.objects.get(criterion1 & criterion2)
            item_field.value = field_data['value']
            item_field.save()

        return instance
