from django.contrib.auth.models import User
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from authentication.models.profile import Profile
from reminder.models.field import Field
from reminder.models.field_type import FieldType


class ItemModelTest(APITestCase):
    def setUp(self):
        self.url = reverse('item-model-list')
        self.correct_username = 'john_doe'
        self.correct_password = 'foobar'
        self.email = 'toto@toto.fr'
        self.user = User.objects.create_user(username=self.correct_username,
                                             password=self.correct_password,
                                             email=self.email)
        self.profile = Profile.objects.create(user=self.user)
        self.wrong_username = 'wrong'
        self.wrong_password = 'wrong'
        self.faker = Faker()
        FieldType.objects.create(label='Texte Court')

    def test_create_item_model(self):
        data = {
            "label": "Anniversaire",
            "fields": [
                {
                    "label": "Nom",
                    "type": "Texte Court"
                },
                {
                    "label": "Prénom",
                    "type": "Texte Court"
                }
            ]
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.url, data)
        model_id = response.data['id']
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(['Nom', 'Prénom'], [field.label for field in
                                             Field.objects.filter(
                                                 model=model_id)])

    def test_add_fields_to_model(self):
        data = {
            "label": "Anniversaire",
            "fields": [
                {
                    "label": "Nom",
                    "type": "Texte Court"
                },
                {
                    "label": "Prénom",
                    "type": "Texte Court"
                }
            ]
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.url, data)
        model_id = response.data['id']

        new_data = {
            "id": model_id,
            "label": "Anniversaire",
            "fields": [
                {
                    "label": "Nom",
                    "type": "Texte Court"
                },
                {
                    "label": "Prénom",
                    "type": "Texte Court"
                },
                {
                    "label": "Nom de jeune fille",
                    "type": "Texte Court"
                }
            ]
        }

        response = self.client.put(reverse('item-model-detail', args=[model_id]),
                        new_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(['Nom', 'Prénom', 'Nom de jeune fille'],
                         [field.label for field in
                          Field.objects.filter(
                              model=model_id)])

    def test_delete_fields_to_model(self):

        data = {
            "label": "Anniversaire",
            "fields": [
                {
                    "label": "Nom",
                    "type": "Texte Court"
                },
                {
                    "label": "Prénom",
                    "type": "Texte Court"
                },
                {
                    "label": "Nom de jeune fille",
                    "type": "Texte Court"
                }
            ]
        }


        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.url, data)
        model_id = response.data['id']

        new_data = {

            "id": model_id,
            "label": "Anniversaire",
            "fields": [
                {
                    "label": "Nom",
                    "type": "Texte Court"
                },
                {
                    "label": "Prénom",
                    "type": "Texte Court"
                }
            ]
        }
        response = self.client.put(reverse('item-model-detail', args=[model_id]),
                        new_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(['Nom', 'Prénom'], [field.label for field in
                                             Field.objects.filter(
                                                 model=model_id)])
