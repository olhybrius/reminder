from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from reminder.models.field_type import FieldType
from reminder.serializers.field_type import FieldTypeSerializer


class FieldTypeViewSet(ReadOnlyModelViewSet):
    queryset = FieldType.objects.all()
    serializer_class = FieldTypeSerializer


class KermitView(APIView):

    def get(self, request):
        return HttpResponse(
            """
                       ---.     .---.         ______________
                     ( -o- )---( -o- )      /              |
                     ;-...-`   `-...-;      |     HEY !    |
                    /                 \\     |______________|              
                   /                   \\   /
                  | /_               _\ |
                  \`'.`'"--.....--"'`.'`/
                   \  '.   `._.`   .'  /
                _.-''.  `-.,,_,,.-`  .''-._
               `--._  `'-.,_____,.-'`  _.--`
                    /                 \\
                   /.-'`\   .'.   /`'-.\\
                  `      '.'   '.' '   
                  
                  
            """, 'text/plain')
