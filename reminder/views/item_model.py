from rest_framework.viewsets import ModelViewSet

from reminder.models.item_model import ItemModel
from reminder.serializers.item_model import ItemModelSerializer


class ItemModelViewSet(ModelViewSet):
    serializer_class = ItemModelSerializer

    def get_queryset(self):
        return ItemModel.objects.filter(profile__user=self.request.user)
