from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from reminder.models.reminder_item import ReminderItem
from reminder.serializers.reminder_item import ReminderItemSerializer


class ReminderItemViewSet(ModelViewSet):
    serializer_class = ReminderItemSerializer

    def get_queryset(self):
        return ReminderItem.objects.filter(
            model__profile__user=self.request.user).distinct()